# Terms and Conditions

The Magento 2 Extractor for KBC is built and offered by Leo as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to extract the data from Magento 2 online to Keboola Connection Platform (KBC). 
API call is process by using user-entered keys for API authentication, no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## Magento Licence Terms
[Official website](https://magento.com/legal/terms)

## Contact Info

Leo Chan  
Vancouver, Canada (PST time)  
email: support@keboola.com  