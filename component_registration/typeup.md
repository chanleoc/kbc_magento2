## Component Configuration
Extract one company's E-Commerce information from Magento 2 Online to KBC.

## Important Parameters
  1. URL
      - The URL of your Magento eCommerce Platform
      - **Please enter your URL input before '/rest/V1/'**
      - Example: 
           - Full Link: http://www.myshop.net/index.php/rest/V1/
           - Input: http://www.myshop.net/index.php
           
  2. Username  
         
  3. Password   

  4. Templates Selection  
      - Carts
      - Categories
      - Coupons
      - Customers
      - Invoices
      - Orders
      - Products
      - Transactions

**Note: Templates can be switched to JSON formatted template for customization**

Detail Magento 2 Online Documentation & Enedpoints can be found [here](http://devdocs.magento.com/swagger/#/)