# README #

This repository is a collection of configurations needed to register Keboola Generic Extractor as a branded Magento 2 KBC Extractor.
Extractor's task is to help user to extract the data from Magento 2 online to Keboola Connection Platform (KBC). 

## API documentation ##
[Magento 2 API documentation](http://devdocs.magento.com/swagger/#/)  

## Configuration ##
  
Required Information:  
   
  1. URL
      - The URL of your Magento eCommerce Platform
      - **Please enter your URL input before '/rest/V1/'**
      - Example: 
           - Full Link: http://www.myshop.net/index.php/rest/V1/
           - Input: http://www.myshop.net/index.php
           
  2. Username  
      - Please ensure your acount has the right privilege to access the defined endpoint
      - Instructions on how to add user or modify users' rights: [Here](https://help.aftership.com/hc/en-us/articles/115008297607-Finding-Magento-API-credentials)
         
  3. Password   

  4. Templates Selection  
      - Carts
      - Categories
      - Coupons
      - Customers
      - Invoices
      - Orders
      - Products
      - Transactions

**Note: Templates can be switched to JSON formatted template for customization**
         
   For overview of the API and list of supported tables, please visit [API Overview](http://devdocs.magento.com/guides/v2.0/rest/bk-rest.html) & [API Explorer](http://devdocs.magento.com/swagger/index_20.html#/). 

## Contact Info ##
Leo Chan  
Vancouver, Canada (PST time)   
Email: leo@keboola.com  
Private: cleojanten@hotmail.com   