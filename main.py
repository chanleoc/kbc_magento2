"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment 
"""

import sys
import io
import logging
import json
import csv
import pandas as pd
from keboola import docker
from pygelf import GelfTcpHandler
import requests


# initialize application
cfg = docker.Config('/data/')

# access the supplied parameters
params = cfg.get_parameters()
URL = cfg.get_parameters()["url"]
USER = cfg.get_parameters()["user"]
PASSWORD = cfg.get_parameters()["#password"]
CLIENT_ID = cfg.get_parameters()["consumer_id"]
CLIENT_SECRET = cfg.get_parameters()["#consumer_secret"]
ACCESS_TOKEN = cfg.get_parameters()["#access_token"]
ACCESS_TOKEN_SECRET = cfg.get_parameters()["#access_token_secret"]
ENDPOINT = cfg.get_parameters()["endpoint"]
#DEBUG = cfg.get_parameters()["debug"]

REST_LINK = "/rest/V1/"
AUTH_LINK = "integration/admin/token"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"
pagesize = 1000

logger = logging.getLogger()
fields = {"_some": {"structured": "data"}}

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")
"""
logger.addHandler(GelfTcpHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=os.getenv('KBC_LOGGER_PORT'),
    debug=True, **fields
    ))
"""

def request_token(url,user,password):
    """
    Request access token
    Output: token(string)
    """
    string = {'username':user,'password':password}
    token_headers = {'Content-Type':'application/json'}
    
    token = requests.post(url, data =json.dumps(string),headers=token_headers)
    token_id = json.loads(token.text)

    return token_id

def data_request(url,token):
    """
    Request data
    Output: data(json)
    """
    requests_headers = {'Content-Type':'application/json','Authorization':''}
    requests_headers['Authorization'] = 'Bearer '+token

    data = requests.get(url,headers=requests_headers)
    result = json.loads(data.text)

    return result

def data_pagination(url,token,currentpage,total_rows):
    """
    Iterate currentpage until there isn't any data left to fetch
    """
    output=[]
    while pagesize*(currentpage-1) <= total_rows:
        params = "?searchCriteria[pageSize]="+str(pagesize)+"&searchCriteria[currentPage]="+str(currentpage)
        request_url = url+params
        logging.info("Request Link: {0}".format(request_url))
        data_json = data_request(request_url, token)        

        for i in data_json['items']:
            output.append(flatten_json(i))
        currentpage+=1
    return output

def output_json(jsonfile,filename):
    """
    Output JSON file to CSV
    """
    output_df = pd.DataFrame(jsonfile)
    output_df.to_csv(DEFAULT_FILE_DESTINATION+filename+".csv",index=False)
    logging.info("Outputing {0}.csv ...".format(filename))

def flatten_json(y):
    """
    # Credits: https://gist.github.com/amirziai/2808d06f59a38138fa2d
    # flat out the json objects
    """
    out = {}
    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '/')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '/')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def main():
    """
    Main execution script.
    Data request per endpoint  
    """
    """
    # Oauth 2 (user/password configuration)
    # Token request
    token_url = URL+REST_LINK+AUTH_LINK
    token = request_token(token_url,USER,PASSWORD)
    # remove quotations from the token
    token = json.loads(token)

    # This use case only applies to magento as access token is a string
    # does not output string if log in fails
    if (type(token)==str):
        logging.info("Access Token Acquired.")
    else:
        logging.error("ERROR: {0}".format(token))
        logging.error("Exit.")
        sys.exit(1)
    """
    for i in ENDPOINT:
        output_name = i['type']
        # Endpoints which have a different endpoint url link
        exceptions = ["customers","carts","saleRules","coupons","taxRates","taxRules","taxClasses"]
        if output_name in exceptions:
            end_point = i['type']+"/search"
        else:
            end_point = i['type']

        # obtain parameters for pagination
        request_url = URL+REST_LINK+end_point
        currentpage = 1
        try:
            # Test connection and fetch total_count
            data_json = data_request(request_url+"?searchCriteria",ACCESS_TOKEN)
            #data_json = oauth1_request(request_url+"?searchCriteria",oauth1_cred)
            logging.info("Endpoint {0} connected.".format(output_name))
        except Exception:
            logging.error("ERROR: Endpoint {0} access declined.".format(output_name))
            logging.error("Please check your credentials.")
            logging.error("Exit.")
            sys.exit(1)

        try:
            # Data request
            # does not return 'total_count' if request is not valid
            total_rows = data_json['total_count']
            if total_rows == 0:
                # if there isnt any data at the endpoint
                logging.info("Endpoint *{0}* - No data.".format(output_name.upper()))
            else:
                # Keep fetching data until number of data fetched equals to total_count
                result_json = data_pagination(request_url,ACCESS_TOKEN,currentpage,total_rows)
                #Output each endpoint file
                output_json(result_json,output_name)            
        except Exception:
            logging.error("ERROR: Endpoint *{0}* - {1}".format(output_name.upper(),data_json))

def main2():

    token_url = URL+REST_LINK+AUTH_LINK
    print("token_url: {0}".format(token_url))
    #sys.exit(0)
    token = request_token(token_url, USER, PASSWORD)
    print("token: {0}".format(token))
    
    #sys.exit(0)
    for i in ENDPOINT:
        end_point = i["type"]
        request_url = URL+REST_LINK+end_point+"?searchCriteria"
        print("request_url: {0}".format(request_url)) 
        sys.exit(0)
        output = data_request(request_url,token)
        print(output)   



if __name__ == "__main__":

    main2()

logging.info("Done.")